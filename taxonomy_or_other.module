<?php

/**
 * @file
 * Defines a field that allow to select a taxonomy term or a text value.
 */

define('TAXONOMY_OR_OTHER_NO_VALUE_SELECTED', '_none');
define('TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED', '_other');

/**
 * Implements hook_field_info().
 *
 * Field settings:
 * - allowed_values: a list array of one or more vocabulary trees:
 *   - vocabulary: a vocabulary machine name.
 *   - parent: a term ID of a term whose children are allowed. This should be
 *     '0' if all terms in a vocabulary are allowed. The allowed values do not
 *     include the parent term.
 */
function taxonomy_or_other_field_info() {
  return array(
    'taxonomy_or_other' => array(
      'label' => t('Term reference or other'),
      'description' => t('This field stores a reference to a taxonomy term or a other text option.'),
      'default_widget' => 'taxonomy_or_other_select',
      'default_formatter' => 'taxonomy_or_other_link',
      'settings' => array(
        'vocabulary' => array(),
        'other_max_length' => 255,
      ),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function taxonomy_or_other_field_settings_form($field, $instance, $has_data) {

  // Get proper values for 'allowed_values_function', which is a core setting.
  $vocabularies = taxonomy_get_vocabularies();

  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->machine_name] = $vocabulary->name;
  }

  $form['vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#default_value' => $field['settings']['vocabulary'],
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t('The vocabulary which supplies the options for this field.'),
  );

  $form['other_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Other text maximum length'),
    '#default_value' => $field['settings']['other_max_length'],
    '#required' => TRUE,
    '#description' => t('The maximum length of the other text field in characters.'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#disabled' => $has_data,
  );

  return $form;
}

/**
 * Field validation handler for options element.
 */
function taxonomy_or_other_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  if ($instance['required']) {
    foreach ($items as $delta => $item) {
      if ((!isset($item['tid']) || $item['tid'] == TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED)
            && empty($item['other'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'required_field',
          'message' => t('!name field is required.', array('!name' => $instance['label'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_widget_info().
 */
function taxonomy_or_other_field_widget_info() {
  return array(
    'taxonomy_or_other_select' => array(
      'label' => t('Select list'),
      'field types' => array('taxonomy_or_other'),
      'settings' => array(
        'default_term' => 0,
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
    'taxonomy_or_other_buttons' => array(
      'label' => t('Radio buttons'),
      'field types' => array('taxonomy_or_other'),
      'settings' => array(
        'default_term' => 0,
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function taxonomy_or_other_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $vocabulary = taxonomy_vocabulary_machine_name_load($field['settings']['vocabulary']);
  $tree = taxonomy_get_tree($vocabulary->vid);

  $taxonomy_widget_type = ($instance['widget']['type'] == 'taxonomy_or_other_select')
      ? 'select' : 'radios';

  $i18n_enabled = FALSE;
  if (module_exists('i18n_taxonomy')) {
    $i18n_enabled = TRUE;
  }
  
  foreach ($tree as $item) {
    $term_name = $i18n_enabled ? i18n_taxonomy_term_name($item) : $item->name;
    if ($taxonomy_widget_type == 'select') {
      $options[$item->tid] = str_repeat('-', $item->depth) . $term_name;
    }
    else {
      $options[$item->tid] = $term_name;
    }
  }
  $options[TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED] = t('Other');

  $element += array(
    '#tree' => TRUE,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'taxonomy_or_other') . '/taxonomy_or_other_' . $taxonomy_widget_type . '.js'),
      'css' => array(drupal_get_path('module', 'taxonomy_or_other') . '/taxonomy_or_other.css'),
    ),
  );

  $term_default_value = NULL;
  if (isset($items[$delta]['tid'])) {
    $term_default_value = $items[$delta]['tid'];
  }
  elseif (isset($items[$delta]['other'])) {
    $term_default_value = TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED;
  }

  $element['#type'] = 'item';

  $element['tid'] = array(
    '#type' => $taxonomy_widget_type,
    '#default_value' => $term_default_value,
    '#options' => $options,
    '#empty_value' => TAXONOMY_OR_OTHER_NO_VALUE_SELECTED,
    '#required' => $instance['required'] ? TRUE : FALSE,
  );

  $element['other'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($items[0]['other']) ? $items[0]['other'] : NULL,
    '#maxlength' => $field['settings']['other_max_length'],
    '#description' => t('Specify your option'),
  );

  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function taxonomy_or_other_field_is_empty($item, $field) {
  if (!is_array($item)) {
    return TRUE;
  }
  if (empty($item['tid'])) {
    return TRUE;
  }
  if ($item['tid'] == TAXONOMY_OR_OTHER_NO_VALUE_SELECTED) {
    return TRUE;
  }
  if ($item['tid'] == TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED
          && empty($item['other'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_presave().
 */
function taxonomy_or_other_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if ($item['tid'] == TAXONOMY_OR_OTHER_OTHER_VALUE_SELECTED) {
      $items[$delta]['tid'] = NULL;
    }
    else {
      $items[$delta]['other'] = NULL;
    }
  }
}


/**
 * Implements hook_field_formatter_info().
 */
function taxonomy_or_other_field_formatter_info() {
  return array(
    'taxonomy_or_other_link' => array(
      'label' => t('Link'),
      'field types' => array('taxonomy_or_other'),
    ),
    'taxonomy_or_other_plain' => array(
      'label' => t('Plain text'),
      'field types' => array('taxonomy_or_other'),
    ),
  );
}

/**
 * Implements hook_field_formatter_prepare_view().
 *
 * This preloads all taxonomy terms for multiple loaded objects at once and
 * unsets values for invalid terms that do not exist.
 */
function taxonomy_or_other_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  $tids = array();

  // Collect every possible term attached to any of the fieldable entities.
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      // Force the array key to prevent duplicates.
      if (!empty($item['tid']) && $item['tid'] != 'autocreate') {
        $tids[$item['tid']] = $item['tid'];
      }
    }
  }
  if ($tids) {
    $terms = taxonomy_term_load_multiple($tids);

    // Iterate through the fieldable entities again to attach the loaded
    // term data.
    foreach ($entities as $id => $entity) {
      $rekey = FALSE;

      foreach ($items[$id] as $delta => $item) {
        // Check whether the taxonomy term field instance value could be loaded.
        if (isset($terms[$item['tid']])) {
          // Replace the instance value with the term data.
          $items[$id][$delta]['taxonomy_term'] = $terms[$item['tid']];
        }
        // Terms to be created are not in $terms, but are still legitimate.
        elseif ($item['tid'] == 'autocreate') {
          // Leave the item in place.
        }
        // Otherwise, unset the instance value, since the term does not exist.
        else {
          unset($items[$id][$delta]);
          $rekey = TRUE;
        }
      }

      if ($rekey) {
        // Rekey the items array.
        $items[$id] = array_values($items[$id]);
      }
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function taxonomy_or_other_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'taxonomy_or_other_link':
      foreach ($items as $delta => $item) {
        if (!empty($item['tid'])) {
          if ($item['tid'] == 'autocreate') {
            $element[$delta] = array(
              '#markup' => check_plain($item['name']),
            );
          }
          else {
            $term = $item['taxonomy_term'];
            $uri = entity_uri('taxonomy_term', $term);
            $element[$delta] = array(
              '#type' => 'link',
              '#title' => $term->name,
              '#href' => $uri['path'],
              '#options' => $uri['options'],
            );
          }
        }
        else {
          $element[$delta] = array(
            '#markup' => check_plain($item['other']),
          );
        }
      }
      break;

    case 'taxonomy_or_other_plain':
      foreach ($items as $delta => $item) {
        $name = NULL;

        if (!empty($item['tid'])) {
          $name = ($item['tid'] != 'autocreate' ? $item['taxonomy_term']->name : $item['name']);
        }
        else {
          $name = $item['other'];
        }

        $element[$delta] = array(
          '#markup' => check_plain($name),
        );
      }
      break;
  }

  return $element;
}
